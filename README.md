## Overview

Project gốc: https://github.com/qwrite/angular-app-kubernetes.git

Project sử dụng Docker, Kubernetes, Helm để CI/CD tự động cho nhiều instance của một image và sử dụng helm để update config.

## Create deployment kubernetes

kubectl apply -f deployment.yaml

## Update image for deployment

kubectl set image deployment/angular-deployment angular=cuongquocvn/angular-app:latest

## Restart deployment kubernetes

kubectl rollout restart deployment angular-deployment
